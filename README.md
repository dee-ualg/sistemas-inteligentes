´ 
software requerido
------------------

- instalação do Python
    - simples a partir do site
    - Anaconda - sistema completo de gestão para python e ferramentas associadas 

- instalar jupyter
    - pip install jupyter
    - usar anaconda e instalar jupyterlab
    - ...
  
configurar jupyter
------------------

- se quiser uma configuração do tema do jupyter
    >>> jt -t grade3 -f fira -fs 13 -nf ptsans -nfs 11 -N -kl -cursw 5 -cursc r -cellw 95% -T

    
iniciar jupyter
---------------

- a partir de um terminal
    >>> jupyter-notebook

- menu windows sobre a instalação do Anaconda


Kaggle
---------------

Requistos para alguns notebooks funcionarem

- criar uma conta no Kaggle
- no profile do utilizador criar uma API key e guardar na pasta adequada o kaggle.json
    - linux: ~/.kaggle/kaggle.json
    - windows (não testado):  C:\Users\<Windows-username>\.kaggle\kaggle.json
